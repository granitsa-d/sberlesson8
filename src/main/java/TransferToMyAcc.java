import entity.Sender;
import terminalServer.NotEnoughMoneyException;

public class TransferToMyAcc extends TerminalImpl{

    public TransferToMyAcc(Sender sender) {
        super(sender);
    }

    @Override
    protected void transfer(int amount) throws NotEnoughMoneyException {
        server.doTransferToAcc(sender, amount);

    }
}