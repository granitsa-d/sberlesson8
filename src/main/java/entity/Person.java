package entity;

import java.math.BigDecimal;
import java.util.Objects;

public class Person {
    private String surname;
    private String cardNumber;
    private long accountNumber;
    private BigDecimal balanceAccount;
    private BigDecimal balanceCard;

    public Person(String surname, String cardNumber, long accountNumber, BigDecimal balanceCard, BigDecimal balanceAccount) {
        this.surname = surname;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.balanceCard = balanceCard;
        this.balanceAccount = balanceAccount;
    }

    public BigDecimal getBalanceAccount() { return balanceAccount; }

    public void setBalanceAccount(BigDecimal balanceAccount) { this.balanceAccount = balanceAccount; }

    public BigDecimal getBalanceCard() {
        return balanceCard;
    }

    public void setBalanceCard(BigDecimal balanceCard) {
        this.balanceCard = balanceCard;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public String getFullName() {
        return surname;
    }

    public boolean isEnoughMoneyAcc(int amount) {
        return getBalanceAccount().compareTo(new BigDecimal(amount)) >= 0;
    }

    public boolean isEnoughMoneyCard(int amount) {
        return getBalanceCard().compareTo(new BigDecimal(amount)) >= 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return accountNumber == person.accountNumber &&
                Objects.equals(cardNumber, person.cardNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardNumber, accountNumber);
    }

    @Override
    public String toString() {
        return "surname='" + surname + '\'' +
                ", balanceCard=" + balanceCard +
                ", balanceAccount=" + balanceAccount;
    }
}
