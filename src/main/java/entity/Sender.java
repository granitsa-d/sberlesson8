package entity;

import java.math.BigDecimal;

public class Sender extends Person{
    private int pin;

    public Sender(String surname, String cardNumber, long accountNumber, BigDecimal balanceCard, BigDecimal balanceAccount, int pin) {
        super(surname, cardNumber, accountNumber, balanceCard, balanceAccount);
        this.pin = pin;
    }

    public int getPin() {
        return pin;
    }

}
