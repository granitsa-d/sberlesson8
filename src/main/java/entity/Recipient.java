package entity;

import java.math.BigDecimal;

public class Recipient extends entity.Person {
    public Recipient(String surname, String cardNumber, long accountNumber, BigDecimal balanceAccount,BigDecimal balanceCard) {
        super(surname, cardNumber, accountNumber, balanceCard, balanceAccount);
    }
}
