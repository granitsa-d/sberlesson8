import entity.Recipient;
import entity.Sender;
import terminalServer.NotEnoughMoneyException;

public class TransferToAnyFromCard extends TerminalImpl{
    private Recipient recipient;

    public TransferToAnyFromCard(Sender sender, Recipient recipient) {
        super(sender);
        this.recipient = recipient;
    }

    @Override
    protected void transfer(int amount) throws NotEnoughMoneyException {
        server.doTransferToCard(sender, recipient, amount);
    }
}

