import entity.Recipient;
import entity.Sender;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Sender sender =
                new Sender("Граница",
                        "d3424j23442",
                        123456789123l,
                        new BigDecimal("51"),
                        new BigDecimal("400"),
                        1234);

        Recipient recipient =
                new Recipient("Федотов",
                        "sdklslj332422d",
                        834728347290l,
                        new BigDecimal("200"),
                        new BigDecimal("300"));

        Terminal terminal = new TransferToMyCard(sender);
        terminal.enterPin(123);
        terminal.doTransfer(100);
        terminal.enterPin(123);
        terminal.enterPin(2344);
        terminal.enterPin(5345);
        Thread.sleep(5000);
        terminal.enterPin(1234);
        terminal.doTransfer(500);
        terminal.doTransfer(1500);
        terminal.doTransfer(345);
        terminal.doTransfer(100);
        System.out.println(sender);
        System.out.println("**************************");
        terminal = new TransferToAnyFromCard(sender, recipient);
        terminal.doTransfer(123);
        terminal.enterPin(1234);
        terminal.doTransfer(200);
        terminal.doTransfer(100);
        System.out.println(sender);
        System.out.println(recipient);



    }
}